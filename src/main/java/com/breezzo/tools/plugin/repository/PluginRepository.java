package com.breezzo.tools.plugin.repository;

import com.breezzo.tools.plugin.PluginInfo;

import java.util.Optional;

/**
 * Created by breezzo on 06.07.16.
 */
public interface PluginRepository<R> {

    R add(PluginInfo pluginInfo);

    <T> Optional<PluginInfo<T>> find(Class<T> plugin);
}
