package com.breezzo.tools.plugin;

/**
 * Created by breezzo on 06.07.16.
 */
public interface PluginConfig {
    String getInterfaceClassName();

    String getImplementationClassName();
}
