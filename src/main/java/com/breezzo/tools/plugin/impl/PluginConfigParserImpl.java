package com.breezzo.tools.plugin.impl;

import com.breezzo.tools.plugin.PluginConfig;
import com.breezzo.tools.plugin.PluginConfigParser;
import com.breezzo.tools.plugin.exception.PluginSystemException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginConfigParserImpl implements PluginConfigParser {
    @Override
    public PluginConfig parse(InputStream configStream) {
        Properties props = load(configStream);

        PluginConfigImpl.Builder configBuilder = new PluginConfigImpl.Builder();
        configBuilder.setInterfaceClassName(props.getProperty("plugin.interface"));
        configBuilder.setImplementationClassName(props.getProperty("plugin.implementation"));

        return configBuilder.build();
    }

    private Properties load(InputStream configStream) {
        try {
            Properties props = new Properties();
            props.load(configStream);
            return props;
        } catch (IOException e) {
            throw new PluginSystemException(e.getMessage(), e);
        }
    }
}
