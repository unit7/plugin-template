package com.breezzo.tools.plugin.impl;

import com.breezzo.tools.plugin.PluginFactory;
import com.breezzo.tools.plugin.PluginInfo;
import com.breezzo.tools.plugin.exception.PluginSystemException;
import com.breezzo.tools.plugin.repository.PluginRepository;
import com.google.common.io.ByteStreams;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.VFSClassLoader;

import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Optional;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginFactoryImpl implements PluginFactory {

    private PluginRepository<?> pluginRepository;

    public PluginFactoryImpl(PluginRepository<?> pluginRepository) {
        this.pluginRepository = pluginRepository;
    }

    @Override
    public <T> T create(Class<T> pluginInterface) {
        if (pluginInterface == null) {
            throw new PluginSystemException("pluginInterface is null");
        }

        Optional<PluginInfo<T>> pluginOptional = pluginRepository.find(pluginInterface);

        if (!pluginOptional.isPresent()) {
            throw new PluginSystemException("Could not find plugin with interface " + pluginInterface.getCanonicalName());
        }

        return makeProxy(pluginOptional.get());
    }

    private <T> T makeProxy(PluginInfo<T> pluginInfo) {
        // TODO верный ли classloader?
        //noinspection unchecked
        return (T) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{pluginInfo.getInterfaceClass()}, new ProxyHandler(pluginInfo));
    }

    private static class ProxyHandler implements InvocationHandler {
        private PluginInfo<?> pluginInfo;

        public ProxyHandler(PluginInfo<?> pluginInfo) {
            this.pluginInfo = pluginInfo;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

            ClassLoader pluginClassLoader = createClassLoader();

            String pluginImplClassName = pluginInfo.getImplementationClassName();
            Class<?> pluginClassImpl = Class.forName(pluginImplClassName, true, pluginClassLoader);
            Object plugin = pluginClassImpl.newInstance();
            Thread currentThread = Thread.currentThread();
            ClassLoader currentClassLoader = currentThread.getContextClassLoader();
            Object result;

            try {
                currentThread.setContextClassLoader(pluginClassLoader);
                result = method.invoke(plugin, args);
            } finally {
                currentThread.setContextClassLoader(currentClassLoader);
            }

            return result;
        }

        private ClassLoader createClassLoader() throws Exception {
            // TODO проверить корректность подхода
            FileSystemManager fsManager = VFS.getManager();
            FileObject jarFile = fsManager.resolveFile("ram://lib.jar");
            OutputStream jarOutputStream = jarFile.getContent().getOutputStream();
            ByteStreams.copy(pluginInfo.getContent(), jarOutputStream);
            jarFile.getContent().close();

            return new VFSClassLoader(jarFile, fsManager, Thread.currentThread().getContextClassLoader());
        }
    }
}
