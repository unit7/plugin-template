package com.breezzo.tools.plugin.impl;

import com.breezzo.tools.plugin.PluginConfig;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginConfigImpl implements PluginConfig {
    private String interfaceClassName;
    private String implementationClassName;

    @Override
    public String getInterfaceClassName() {
        return interfaceClassName;
    }

    @Override
    public String getImplementationClassName() {
        return implementationClassName;
    }

    public PluginConfigImpl setInterfaceClassName(String interfaceClassName) {
        this.interfaceClassName = interfaceClassName;
        return this;
    }

    public PluginConfigImpl setImplementationClassName(String implementationClassName) {
        this.implementationClassName = implementationClassName;
        return this;
    }

    static class Builder {
        private String interfaceClassName;
        private String implementationClassName;

        Builder setInterfaceClassName(String interfaceClassName) {
            this.interfaceClassName = interfaceClassName;
            return this;
        }

        Builder setImplementationClassName(String implementationClassName) {
            this.implementationClassName = implementationClassName;
            return this;
        }

        PluginConfig build() {
            PluginConfigImpl config = new PluginConfigImpl();
            config.setImplementationClassName(implementationClassName);
            config.setInterfaceClassName(interfaceClassName);
            return config;
        }
    }
}
