package com.breezzo.tools.plugin.exception;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginSystemException extends RuntimeException {
    public PluginSystemException() {
    }

    public PluginSystemException(String message) {
        super(message);
    }

    public PluginSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginSystemException(Throwable cause) {
        super(cause);
    }

    public PluginSystemException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
