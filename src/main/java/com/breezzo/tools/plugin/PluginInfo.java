package com.breezzo.tools.plugin;

import java.io.InputStream;

/**
 * Created by breezzo on 06.07.16.
 */
public interface PluginInfo<T> {
    InputStream getContent();

    Class<T> getInterfaceClass();

    String getImplementationClassName();
}
