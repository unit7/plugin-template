package com.breezzo.tools.plugin;

import java.io.InputStream;

/**
 * Created by breezzo on 06.07.16.
 */
public interface PluginConfigParser {

    PluginConfig parse(InputStream configStream);
}
