package com.breezzo.tools.plugin.generic;

import com.breezzo.tools.plugin.PluginFactory;
import com.breezzo.tools.plugin.impl.PluginFactoryImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginGenericTest {

    @Test
    public void testInvoke() {
        PluginFactory pluginFactory = new PluginFactoryImpl(new PluginRepositoryTest());
        Plugin plugin = pluginFactory.create(Plugin.class);
        String result = plugin.invoke("hello from invoker");
        Assert.assertEquals("invoked", result);
    }
}
