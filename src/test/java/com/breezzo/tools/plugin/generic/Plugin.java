package com.breezzo.tools.plugin.generic;

/**
 * Created by breezzo on 06.07.16.
 */
public interface Plugin {
    String invoke(Object param);
}
