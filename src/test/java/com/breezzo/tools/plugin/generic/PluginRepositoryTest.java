package com.breezzo.tools.plugin.generic;

import com.breezzo.tools.plugin.PluginInfo;
import com.breezzo.tools.plugin.repository.PluginRepository;

import java.io.*;
import java.util.Optional;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginRepositoryTest implements PluginRepository<Object> {

    @Override
    public Object add(PluginInfo pluginInfo) {
        return null;
    }

    @Override
    public <T> Optional<PluginInfo<T>> find(Class<T> plugin) {
        return Optional.of(
                new PluginInfo<T>() {

                    @Override
                    public InputStream getContent() {
                        try {
                            return jarInputStream();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public Class<T> getInterfaceClass() {
                        //noinspection unchecked
                        return (Class<T>) Plugin.class;
                    }

                    @Override
                    public String getImplementationClassName() {
                        return "com.breezzo.tools.plugin.generic.PluginImpl";
                    }
                });
    }

    private InputStream jarInputStream() {
        return getClass().getResourceAsStream("/lib.jar");
    }
}
