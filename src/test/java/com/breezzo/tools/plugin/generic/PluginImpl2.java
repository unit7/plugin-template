package com.breezzo.tools.plugin.generic;

import com.google.common.base.Preconditions;
import com.google.common.io.ByteStreams;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by breezzo on 06.07.16.
 */
public class PluginImpl2 implements Plugin {
    @Override
    public String invoke(Object param) {
        System.out.println(param);
        loadResource();
        return "invoked";
    }

    private void loadResource() {
        try (InputStream stream = getClass().getResourceAsStream("/resources/test.txt")) {
            byte[] bytes = ByteStreams.toByteArray(stream);
            System.out.println("bytes " + bytes);
            System.out.println("bytes len " + bytes.length);
            String str = new String(bytes).trim();
            System.out.println("data " + str);
            Preconditions.checkArgument("test".equals(str));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
